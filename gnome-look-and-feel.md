# Setting the look and feel of the gnome shell (3.16) #

The gnome shell css is `$XDG_CONFIG_HOME/gtk-3.0/gtk.css`, by default `$XDG_CONFIG_HOME` is not always set, in this case the `gtk.css` is located at `~/.config/gtk-3.0/`.

To reduce the title bar size and make title bar prettier use the css provided.
