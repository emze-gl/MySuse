set number
set cursorline
set signcolumn=yes

set tabstop=4
set shiftwidth=4
set expandtab

set autoindent
set smartindent

set scrolloff=3
set clipboard+=unnamedplus  " copy to system clipboard
set inccommand=split
set mouse=a     " use mouse for e.g. selecting without line numbers

syntax on

"""""""""""
" mappings
"""""""""""
let mapleader = ","
let maplocalleader = "\\"

"""""""""""""""""""""""
" NERDCommenter
"""""""""""""""""""""""
filetype plugin on
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1

"""""""""""""""""""
" Denite 
"""""""""""""""""""
autocmd FileType denite call s:denite_my_settings()

function! s:denite_my_settings() abort
  nnoremap <silent><buffer><expr> <CR>
        \ denite#do_map('do_action')
  nnoremap <silent><buffer><expr> <C-v>
        \ denite#do_map('do_action', 'vsplit')
  nnoremap <silent><buffer><expr> d
        \ denite#do_map('do_action', 'delete')
  nnoremap <silent><buffer><expr> p
        \ denite#do_map('do_action', 'preview')
  nnoremap <silent><buffer><expr> <Esc>
        \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> q
        \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> i
        \ denite#do_map('open_filter_buffer')
endfunction

autocmd FileType denite-filter call s:denite_filter_my_settings()

function! s:denite_filter_my_settings() abort
  imap <silent><buffer> <C-o> <Plug>(denite_filter_quit)
endfunction

nnoremap <C-p> :<C-u>Denite -start-filter file/rec<CR>
nnoremap <leader>b :<C-u>Denite buffer<CR>
nnoremap <leader>s :<C-u>Denite -start-filter grep:::!<CR>
nnoremap <leader>8 :<C-u>DeniteCursorWord grep:.<CR>

"""""""""""
" IDEA like
"""""""""""
nnoremap <c-y> dd
inoremap <c-y> <esc>ddi

"""""""""""""""
" other maps
"""""""""""""""
" JSON formatting
nnoremap <silent> <leader>f :call CocAction('format')<CR>

nnoremap <esc> :noh<return><esc>
nnoremap <leader>w :w<CR>
nnoremap <leader>l :bp<CR>
nnoremap <leader>r :bn<CR>

" Install vim-plug if not already installed
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.nvim/plugged')
Plug 'hashivim/vim-terraform'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'dominikduda/vim_current_word'
Plug 'machakann/vim-highlightedyank'
Plug 'scrooloose/nerdcommenter'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'Yggdroot/indentLine'
Plug 'cespare/vim-toml'
Plug 'flazz/vim-colorschemes'
Plug 'jiangmiao/auto-pairs'
Plug 'elixir-editors/vim-elixir'
Plug 'Shougo/denite.nvim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'neoclide/coc.nvim', {'branch': 'release'}

let g:deoplete#enable_at_startup = 0
let g:deoplete#enable_camel_case = 1
" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_theme='deus'
call plug#end()

""""""""""""""
" coc.nvim
""""""""""""""
" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

:nmap <leader>e :CocCommand explorer<CR>

""""""""""""""""""""
" Colors
""""""""""""""""""""
colorscheme monokain

" set indent line color - for plugin indentLine
let g:indentLine_color_term = 238
let g:limelight_conceal_ctermfg = 241
let g:limelight_conceal_guifg = '#626262'

"""""""""""""""""""
" vim_current_word
"""""""""""""""""""
hi CurrentWord cterm=underline
hi CurrentWordTwins ctermbg=236

hi MatchParen cterm=underline ctermbg=none ctermfg=none

:command C let @/=""

" Reset terminal cursor to blinking underscore
au VimLeave * set guicursor=a:hor10-blinkon1
