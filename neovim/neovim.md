# Neovim #

By default, Neovim's configuration file is `$XDG_CONFIG_HOME/nvim/init.vm`.

If the above mentioned environment variable is not set, the best is to put it in `~/.bashrc`

The plugins should be copied to the `$VIMRUNTIME/plugin` directory.

The syntax files should be copied to the `$VIMRUNTIME/syntax` directory.

The documentation txt (along with it's tag file) should be copied to `$VIMRUNTIME/doc/` after that, you should run `:helptags $VIMRUNTIME/doc`, write permission is needed for that directory.

For plugin management, I use [plug.vim](https://github.com/junegunn/vim-plug)

coc.nvim extensions: coc-java, coc-lua, coc-rls, coc-explorer, coc-json

