# Wifi # 

If the wifi is hard blocked (this can checked with the command `rfkill list` as root). Try to remove the vendor wmi module (in my case this was acer_wmi) by blacklisting that mod:

Insert `blacklist acer_wmi` at the end of `/etc/modprobe.d/50-blacklist.conf`.

For the installer, the following kernel parameter could be inserted:
`modprobe.blacklist=acer_wmi`
