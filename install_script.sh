#!/usr/bin/env sh

zypper install -y git neovim tlp powertop gnome-tweak-tool mc tilix
zypper remove -y --clean-deps tracker evolution evolution-data-server deja-dup gnome-weather

zypper addlock tracker evolution evolution-data-server deja-dup

systemctl mask postfix.service
systemctl stop postfix.service

systemctl mask ModemManager.service
systemctl stop ModemManager.service

systemctl mask wicked.service
systemctl stop wicked.service

systemctl mask iscsid.service
systemctl stop iscsid.service

systemctl mask iscsi.service
systemctl stop iscsi.service

systemctl mask iscsid.socket
systemctl stop iscsid.socket

systemctl mask iscsi.socket
systemctl stop iscsi.socket

