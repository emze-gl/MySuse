# Steps to be able to set backlight with hotkeys.

Acer Aspire 4750g with two video cards.

By default, the hotkeys for backlight-adjustments set the brightness in `/sys/class/backlight/acpi_video0`, however the right one would be in `/sys/class/backlight/intel_backlight`.

For that, the following kernel parameter should be added:
`acpi_backlight=vendor`

Add that to `/etc/default/grub` and invoke the following command to update grub2:
`grub2-mkconfig -o /boot/grub2/grub.cfg`
